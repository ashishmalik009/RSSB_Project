package pos.rssb.com.posrssb;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {
    private WebView webviewObj;
    String ipAddress = "10.1.0.1:9000";
    String url = "";
    String ip = "";
    Handler bluetoothIn;
    private boolean isNewFileNeeded = false;
    int lineNumber;
    final int handlerState = 0;                         //used to identify handler message
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();

    private ConnectedThread mConnectedThread;

    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // String for MAC address
    private static String address;
    private String id, SName, CName;

    private static final int SETTINGS_RESULT = 1;
    private static final int COUNTER_RESULT = 2;
    private TextView bluetoothTextView;
    public static String CANTEEN_DATA = "canteen_data";
    private File mainFolder;

    private EditText etToken, etAmount, etContactNo, etCardId, etCardBalance, etRefundAbleSecurityAmount;

    private String AreaCode, CenterCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getIntentData();

        etToken = (EditText) findViewById(R.id.et_token_number);
        etContactNo = (EditText) findViewById(R.id.et_contact_no);
        etCardId = (EditText) findViewById(R.id.et_Card_Id);
        etCardBalance = (EditText) findViewById(R.id.et_card_balance);
        etRefundAbleSecurityAmount = (EditText) findViewById(R.id.et_refundable_security_amount);
        etAmount = (EditText) findViewById(R.id.et_amout);


        etCardBalance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                long cardBalance = 0;
                if (!etCardBalance.getText().toString().trim().isEmpty())
                    cardBalance = Long.parseLong(etCardBalance.getText().toString());
                else
                    cardBalance = 0;

                String refundableAmount = etRefundAbleSecurityAmount.getText().toString().trim();
                long refundAbleSecurity = 0;
                if(refundableAmount != null && !refundableAmount.isEmpty()) {
                    refundAbleSecurity = Long.parseLong(refundableAmount);
                }


                long Total = cardBalance + refundAbleSecurity;

                etAmount.setText("" + Total);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.btn_sumbit).

                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        etToken.requestFocus();
                        if (!etToken.getText().toString().trim().isEmpty()
                                //&& !etContactNo.getText().toString().trim().isEmpty()
                                && !etCardId.getText().toString().trim().isEmpty()
                                && !etRefundAbleSecurityAmount.getText().toString().trim().isEmpty()
                                && !etCardBalance.getText().toString().trim().isEmpty()
                                && !etAmount.getText().toString().trim().isEmpty()) {

                            if (Build.VERSION.SDK_INT >= 23) {
                                //do your check here
                                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                                    insertData();
                                } else {
                                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                }
                            } else {
                                insertData();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Enter Details", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        //bluetoothTextView = (TextView) findViewById(R.id.bluetooth_data_tx);

      /*  webviewObj = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = webviewObj.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webviewObj.setWebViewClient(new MyBrowser());
        webviewObj.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        Toast.makeText(MainActivity.this, "loading",
                Toast.LENGTH_LONG).show();
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        if (sharedPrefs.getString("url_key", "") == "") {
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString("url_key", ipAddress);
            editor.apply();
            url = "http://" + ipAddress + "/WebPages/Login.aspx";
            Toast.makeText(MainActivity.this, url,
                    Toast.LENGTH_LONG).show();
        } else if (ipAddress == sharedPrefs.getString("url_key", "")) {
            url = "http://" + ipAddress + "/WebPages/Login.aspx";
            Toast.makeText(MainActivity.this, url,
                    Toast.LENGTH_LONG).show();
        } else {
            ipAddress = sharedPrefs.getString("url_key", "");
            url = "http://" + ipAddress + "/WebPages/Login.aspx";
            Toast.makeText(MainActivity.this, url,
                    Toast.LENGTH_LONG).show();
        }
        webviewObj.loadUrl(url);

*/

        bluetoothIn = new

                Handler() {
                    public void handleMessage(android.os.Message msg) {
                        if (msg.what == handlerState) {                                        //if message is what we want
                            String readMessage = (String) msg.obj;

                            if (readMessage.contains("C") || (readMessage.contains("c"))) {
                                recDataString.delete(0, recDataString.length());
                            }

                            // msg.arg1 = bytes from connect thread
                            recDataString.append(readMessage);


                        }
                    }
                }

        ;


        try {
            lineNumber = CsvFileWriter.getLineNumber(getNewFileName());
        } catch (IOException e) {
            e.printStackTrace();
        }

        showAndSetTitle(lineNumber);
        Log.e("MainActivity", "Line : " + lineNumber);

    }

    private void showAndSetTitle(int lineNumeber) {
        setTitle("RSSB-POS - " + lineNumeber);
    }

    private File getMainFolder() {
        if(mainFolder == null) {
            mainFolder = new File(Environment.getExternalStorageDirectory()
                    + "/RSSB-SMC-Returns");

            boolean var = false;
            if (!mainFolder.exists())
                var = mainFolder.mkdir();
            System.out.println("" + var);
        }
        return mainFolder;
    }

    private File getNewFileName() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String fileFolderName = getMainFolder().toString() + "/" + sdf.format(new Date(Calendar.getInstance().getTimeInMillis())).toString();
        File folder = new File(fileFolderName);
        if (!folder.exists())
            folder.mkdir();

        File [] files = folder.listFiles();
        int fileCount = 0;
        if(files != null) {
            fileCount = files.length;
        }
        String filePostFix = "";
        File file;
        if(isNewFileNeeded) {
            filePostFix = "" + (fileCount);
        } else {
            filePostFix = fileCount == 0 || fileCount == 1 ? "" : "" + (fileCount - 1);
        }
        file = new File(folder.getAbsolutePath() + "/" + AreaCode + "_" + CenterCode + filePostFix +".csv");
        Log.e("MainActivity", "File : " + file.getAbsolutePath());
        return file;
    }


    /*public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webviewObj.canGoBack()) {
            webviewObj.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }
*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refresh_menu, menu);

        return true;
    }


    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_refreshBT) {
            if (recDataString != null) {
                String value = recDataString.toString().replace("\r\n", "");

                etToken.setText("");
                etAmount.setText("");
                etContactNo.setText("");
                etCardId.setText("");
                etCardBalance.setText("");

                etToken.requestFocus();
            }/* webviewObj.evaluateJavascript("SetAmountAndBalance('" + value + "')", new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {

                }
            });
            Toast.makeText(MainActivity.this, recDataString, Toast.LENGTH_LONG).show();
            recDataString.delete(0, recDataString.length());*/
        } else if(id == R.id.action_Add) {
            isNewFileNeeded = true;
            lineNumber = 1;
            showAndSetTitle(lineNumber);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

       /* if (requestCode == SETTINGS_RESULT) {
            displayUserSettings();
        }
        else if (requestCode == COUNTER_RESULT){
                if (resultCode == RESULT_OK){
                    String ipAddressFromCounterScreen = data.getStringExtra("ipAddressFromCounterScreen");
                    url = "http://" + ipAddressFromCounterScreen +":9000" + "/WebPages/Login.aspx";
                    webviewObj.loadUrl(url);
                    String ip = ipAddressFromCounterScreen;
                    this.setTitle("POS - " + ip);
                    TextView myAwesomeTextView = (TextView)findViewById(R.id.textView);
                    myAwesomeTextView.setText("POS - " + ip);
                    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString("url_key", ip+":9000");
                    editor.apply();
                    ipAddress = sharedPrefs.getString("url_key", "");


                }
        }*/

    }


    /*private void displayUserSettings() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        ipAddress = sharedPrefs.getString("url_key", "");
        Toast.makeText(MainActivity.this, "Loading",
                Toast.LENGTH_LONG).show();
        url = "http://" + ipAddress + "/WebPages/Login.aspx";
        Toast.makeText(MainActivity.this, url,
                Toast.LENGTH_LONG).show();
        webviewObj.loadUrl(url);
        String address = ipAddress;
        String[] split = address.split(":");
        String ip = split[0];
        this.setTitle("POS - " + ip);
        TextView myAwesomeTextView = (TextView)findViewById(R.id.textView);
        myAwesomeTextView.setText("POS - " + ip);


    }
*/
    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connecetion with BT device using UUID
    }

    public void onResume() {
        super.onResume();

        try {
            //Get MAC address from DeviceListActivity via intent
            Intent intent = getIntent();

            //Get the MAC address from the DeviceListActivty via EXTRA
            address = intent.getStringExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
            if (address != null) {

                // btAdapter = BluetoothAdapter.getDefaultAdapter();      // get Bluetooth adapter

                //create device and set the MAC address
                BluetoothDevice device = btAdapter.getRemoteDevice(address);
                try {
                    btSocket = createBluetoothSocket(device);
                } catch (IOException e) {
                    Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
                }
                // Establish the Bluetooth socket connection.
                try {
                    btSocket.connect();
                } catch (IOException e) {
                    try {
                        btSocket.close();
                    } catch (IOException e2) {
                        //insert code to deal with this
                    }
                }
                mConnectedThread = new ConnectedThread(btSocket);
                mConnectedThread.start();

                //I send a character when resuming.beginning transmission to check device is connected
                //If it is not an exception will be thrown in the write method and finish() will be called
                mConnectedThread.write("x");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            //Don't leave Bluetooth sockets open when leaving activity
            if (btSocket != null) {
                btSocket.close();
            }
        } catch (IOException e2) {
            //insert code to deal with this
        }
    }

//    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
//    private void checkBTState() {
//
//        if (btAdapter == null) {
//            Toast.makeText(getBaseContext(), "Device does not support bluetooth", Toast.LENGTH_LONG).show();
//        } else {
//            if (btAdapter.isEnabled()) {
//            } else {
//                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                startActivityForResult(enableBtIntent, 1);
//            }
//        }
//    }


    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            // Keep looping to listen for received messages
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    // Send the obtained bytes to the UI Activity via handler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                finish();

            }
        }

    }


    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra("Bundle");
        Model model = bundle.getParcelable("SelectAreaName");
        AreaCode = model.getAreaName();
        CenterCode = model.getCenterName();

    }

    private void insertData() {
        File folder = getMainFolder();

//        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
//        String fileName = folder.toString() + "/" + sdf.format(new Date(Calendar.getInstance().getTimeInMillis())).toString() + "_" + AreaCode + "_" + CenterCode + ".csv";
//        File file = new File(fileName);
//
//        System.out.println("Write CSV file:");
//
//        if (file.exists()) {
////            if (countLines(fileName) > 25) {
//            if(isNewFileNeeded) {
//                int fileNumber = folder.list().length;
//                fileName = folder.toString() + "/" + sdf.format(new Date(Calendar.getInstance().getTimeInMillis())).toString() + "_" + AreaCode + "_" + CenterCode + "_"+fileNumber + ".csv";
//                file = new File(fileName);
//            }
//        }

        File file = getNewFileName();
        // reset this flag to keep this new file for newer transactions
        isNewFileNeeded = false;
        lineNumber = 1;
        try {
            lineNumber = CsvFileWriter.getLineNumber(file);
            showAndSetTitle(lineNumber);
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean isFileWrite = CsvFileWriter.writeCsvFile(file.getAbsolutePath(), file.exists(), ""+lineNumber, etCardId.getText().toString(), etContactNo.getText().toString(), etToken.getText().toString(), etCardBalance.getText().toString(), etRefundAbleSecurityAmount.getText().toString(), etAmount.getText().toString());
        if (isFileWrite) {
            Toast.makeText(MainActivity.this, R.string.text_submit_success, Toast.LENGTH_SHORT).show();

            etToken.setText("");
            etAmount.setText("");
            etContactNo.setText("");
            etCardId.setText("");
            etCardBalance.setText("");
            etToken.requestFocus();

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            insertData();
            //resume tasks needing this permission
        }
    }

    public int countLines(String filename) {
        LineNumberReader reader = null;
        int cnt = 0;
        try {
            reader = new LineNumberReader(new FileReader(filename));

            String lineRead = "";
            while ((lineRead = reader.readLine()) != null) {
            }

            cnt = reader.getLineNumber();
            reader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cnt;
    }
}
