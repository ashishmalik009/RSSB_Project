package pos.rssb.com.posrssb;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * Created by appinventiv-pc on 3/5/18.
 */

class CsvFileWriter {

    private static final String COMMA_DELIMITER = ",";

    private static final String NEW_LINE_SEPARATOR = "\n";


    //CSV file header

    private static final String FILE_HEADER = "Serial No,Card Holder Name,Mobile Number,Smart Card Serial Number,Unspent Balance,Security,Total Value Refunded";


    public static boolean writeCsvFile(String fileName, boolean isFileExist, String serialNo,String cardHolderName,String mobileNo,String cardNumber,String balance,String security,String totalAmount) {

        FileWriter fileWriter = null;

        try {

            fileWriter = new FileWriter(fileName, true);

            //Write the CSV file header
            if (!isFileExist) {
                fileWriter.append(FILE_HEADER);


                //Add a new line separator after the header

                fileWriter.append(NEW_LINE_SEPARATOR);
            }

            //Write a new student object list to the CSV file

            fileWriter.append(serialNo);

            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(cardHolderName);

            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(mobileNo);

            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(cardNumber);

            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(balance);

            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(security);

            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(totalAmount);

            fileWriter.append(COMMA_DELIMITER);

            fileWriter.append(NEW_LINE_SEPARATOR);

            System.out.println("CSV file was created successfully !!!");
            return true;


        } catch (Exception e) {

            System.out.println("Error in CsvFileWriter !!!");

            e.printStackTrace();
            return false;

        } finally {

            try {


                if (fileWriter != null)
                    fileWriter.flush();
                if (fileWriter != null)

                    fileWriter.close();

            } catch (IOException e) {

                System.out.println("Error while flushing/closing fileWriter !!!");

                e.printStackTrace();

            }

        }

    }


    public static int getLineNumber(File file) throws FileNotFoundException, IOException{
        FileInputStream is;
        BufferedReader reader;
        int lineNumber = 0;
        if (file.exists()) {
            is = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(is));
            String line = reader.readLine();
            while(line != null){
                Log.d("StackOverflow", line);
                line = reader.readLine();
                lineNumber++;
            }
        }

        return lineNumber == 0 ? 1 : lineNumber ;
    }

}

