package pos.rssb.com.posrssb;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<Model> {

    LayoutInflater flater;

    public CustomAdapter(Activity context, int resouceId, int textviewId, List<Model> list) {

        super(context, resouceId, textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Model model = getItem(position);

        View rowview = flater.inflate(R.layout.custom_list, null, true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.tv_center_name);
        txtTitle.setText(model.getCenterName());


        return rowview;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Model model = getItem(position);

         convertView = flater.inflate(R.layout.custom_list, null, true);

        TextView txtTitle = (TextView) convertView.findViewById(R.id.tv_center_name);
        txtTitle.setText(model.getCenterName());


        return convertView;

    }
}
