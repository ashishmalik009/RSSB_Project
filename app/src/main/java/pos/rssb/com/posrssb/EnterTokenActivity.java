package pos.rssb.com.posrssb;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import pos.rssb.com.posrssb.databinding.ActivityEnterTokenBinding;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func2;

public class EnterTokenActivity extends AppCompatActivity implements View.OnClickListener {

    private Observable<CharSequence> tokenNumberOnservable;
    private Observable<CharSequence> amountobservable;
    private ActivityEnterTokenBinding activityEnterTokenBinding;
    private String id, SName, CName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_token);
        activityEnterTokenBinding = DataBindingUtil.setContentView(this, R.layout.activity_enter_token);
        activityEnterTokenBinding.btnSumbit.setOnClickListener(this);
        activityEnterTokenBinding.btnSumbit.setOnClickListener(this);
        tokenNumberOnservable = RxTextView.textChanges(activityEnterTokenBinding.etTokenNumber).skip(1);
        amountobservable = RxTextView.textChanges(activityEnterTokenBinding.etAmout).skip(1);
        getIntentData();
        setValidation();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        id = bundle.getString("id", "");
        SName = bundle.getString("SName", "");
        CName = bundle.getString("CName", "");
        Log.e("name", CName);

    }

    private void setValidation() {
        tokenNumberOnservable.doOnNext(new Action1<CharSequence>() {
            @Override
            public void call(CharSequence charSequence) {

            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CharSequence>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(CharSequence charSequence) {

                    }
                });


        amountobservable.doOnNext(new Action1<CharSequence>() {
            @Override
            public void call(CharSequence charSequence) {

            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CharSequence>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(CharSequence charSequence) {
                        Log.e("tag", charSequence.toString());
                    }
                });


        Observable.combineLatest(tokenNumberOnservable, amountobservable, new Func2<CharSequence, CharSequence, Boolean>() {
            @Override
            public Boolean call(CharSequence charSequence, CharSequence charSequence2) {
                boolean isValidToken = validate(charSequence.toString());
                boolean isSNameAmount = validate(charSequence2.toString());
                return isValidToken && isSNameAmount;
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Boolean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Boolean validFields) {
                if (validFields) {
                    activityEnterTokenBinding.btnSumbit.setEnabled(true);
                } else {
                    activityEnterTokenBinding.btnSumbit.setEnabled(false);
                }
            }
        });


    }


    private boolean validate(String email) {
        return !TextUtils.isEmpty(email);
    }

    @Override
    public void onClick(View v) {
        if (Build.VERSION.SDK_INT >= 23) {
            //do your check here
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                insertData();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        } else {
            insertData();
        }
    }

    private void insertData() {
        File folder = new File(Environment.getExternalStorageDirectory()
                + "/RSSB Token Record");

        boolean var = false;
        if (!folder.exists())
            var = folder.mkdir();
        System.out.println("" + var);

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String fileName = folder.toString() + "/" + sdf.format(new Date(Calendar.getInstance().getTimeInMillis())).toString() + ".csv";
        File file = new File(fileName);

        System.out.println("Write CSV file:");
       /// boolean isFileWrite = CsvFileWriter.writeCsvFile(fileName, file.exists(), id, SName, CName, activityEnterTokenBinding.etTokenNumber.getText().toString(), activityEnterTokenBinding.etAmout.getText().toString());
        if (true) {
            Toast.makeText(EnterTokenActivity.this, R.string.text_submit_success, Toast.LENGTH_SHORT).show();
            activityEnterTokenBinding.etAmout.setText("");
            activityEnterTokenBinding.etTokenNumber.setText("");
            activityEnterTokenBinding.etTokenNumber.requestFocus();

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            insertData();
            //resume tasks needing this permission
        }
    }
}
