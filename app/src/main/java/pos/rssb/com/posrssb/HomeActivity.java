package pos.rssb.com.posrssb;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import pos.rssb.com.posrssb.databinding.ActivityHomeBinding;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {


    private ActivityHomeBinding activityMainBinding;
    private Activity mActivity;
    private ArrayList<Model> dataList;
    Spinner spinner;

    private int selectedAreaPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
    }

    private void init() {
        dataList = new ArrayList<>();
        setData();

        mActivity = HomeActivity.this;
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        activityMainBinding.btnNext.setOnClickListener(this);


        spinner = (Spinner) findViewById(R.id.sp_center_name);
        CustomAdapter adapter = new CustomAdapter(HomeActivity.this,
                R.layout.custom_list, R.id.tv_center_name, dataList);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedAreaPosition = position;
//                if (selectedAreaPosition <= 2) {
//                    activityMainBinding.etAreaName.setText("");
//                } else {
                    activityMainBinding.etAreaName.setText(dataList.get(selectedAreaPosition).getAreaName());
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        activityMainBinding.tvDate.setText(getDate());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next: {
                if (validateData()) {
                    Intent intent = new Intent(mActivity, DeviceListActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putParcelable("SelectAreaName", dataList.get(selectedAreaPosition));
                    mBundle.putString("Id", activityMainBinding.etId.getText().toString());
                    mBundle.putString("SName", activityMainBinding.etSewadarName.getText().toString());
                    mBundle.putString("Date", activityMainBinding.tvDate.getText().toString());
                    intent.putExtra("Bundle", mBundle);
                    startActivity(intent);
                    finish();
                }
            }
        }
    }

    private boolean validateData() {
        if (spinner.getSelectedItemPosition() == 0) {
            Toast.makeText(HomeActivity.this, "Select Center Name", Toast.LENGTH_LONG).show();
            return false;
        }
        if (activityMainBinding.etId.getText().toString().isEmpty()) {
            Toast.makeText(HomeActivity.this, "Enter Sewadar ID", Toast.LENGTH_LONG).show();
            return false;
        }
        if (activityMainBinding.etSewadarName.getText().toString().isEmpty()) {
            Toast.makeText(HomeActivity.this, "Enter Sewadar Name", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private void setData() {
        Model model = new Model();

        model.setCenterName("Select Center Name");
        model.setCenterCode("");
        model.setAreaName("");
        model.setAreaCode("");

        dataList.add(model);

        model = new Model();

        model.setCenterName("Bhati");
        model.setCenterCode("BH");
        model.setAreaName("Bhati");
        model.setAreaCode("BH");

        dataList.add(model);

        model = new Model();

        model.setCenterName("Pusa Road");
        model.setCenterCode("PR");
        model.setAreaName("Pusa Road");
        model.setAreaCode("PR");

        dataList.add(model);

        model = new Model();
        model.setCenterName("DLF");
        model.setCenterCode("DLF");
        model.setAreaName("Faridabad");
        model.setAreaCode("FB");

        dataList.add(model);

        model = new Model();
        model.setCenterName("Geeta Colony");
        model.setCenterCode("GCL");
        model.setAreaName("Delhi");
        model.setAreaCode("DC");

        dataList.add(model);

    }

    private String getDate()
    {
        SimpleDateFormat sdf=new SimpleDateFormat("dd MMM yyyy");
        return sdf.format(new Date(System.currentTimeMillis()));
    }
}
