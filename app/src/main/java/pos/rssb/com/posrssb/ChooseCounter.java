package pos.rssb.com.posrssb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import android.content.Intent;


public class ChooseCounter extends AppCompatActivity {
    private Spinner chooseCanteenSpinner, chooseCounterSpinner;
    private Button btnSubmit;
    private String ipAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_counter);
        chooseCanteenSpinner = (Spinner) findViewById(R.id.spinner1);
        chooseCounterSpinner = (Spinner) findViewById(R.id.spinner2);
        Intent intent = getIntent();
        ipAddress = intent.getStringExtra(MainActivity.CANTEEN_DATA);
        if(!ipAddress.equals("")){
            String address = ipAddress;
            String addressWithoutPort = address.substring(0, address.indexOf(":"));
            String[] parts = addressWithoutPort.split("\\.");
            String canteenCount = parts[2];
            String counterCount = parts[3];
            if (parts[0].equals("10")){
                int counterValue = Integer.parseInt(counterCount);
                chooseCounterSpinner.setSelection(counterValue);
//            String canteenCount = addressWithoutPort.substring(addressWithoutPort.indexOf("."));
                switch (canteenCount){
                    case "0": chooseCanteenSpinner.setSelection(1);
                        break;
                    case "1": chooseCanteenSpinner.setSelection(2);
                        break;
                    case "4": chooseCanteenSpinner.setSelection(3);
                        break;
                    case "5": chooseCanteenSpinner.setSelection(4);
                        break;
                    case "17": chooseCanteenSpinner.setSelection(5);
                        break;
                    case "25": chooseCanteenSpinner.setSelection(6);
                        break;
                    case "26": chooseCanteenSpinner.setSelection(7);
                        break;
                    case "27": chooseCanteenSpinner.setSelection(8);
                        break;
                    case "28": chooseCanteenSpinner.setSelection(9);
                        break;
                    case "29": chooseCanteenSpinner.setSelection(10);
                        break;

                }
            }


        }

        addListenerOnButton();
    }

    public void addListenerOnButton() {



        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String ipInitials = "10.1.";
                String charFromCanteenSpinner = "";
                String txtFromCanteenSpinner = chooseCanteenSpinner.getSelectedItem().toString();
                String txtFromCounterSpinner = chooseCounterSpinner.getSelectedItem().toString();
                if (txtFromCanteenSpinner.equals("Select Canteen")){
                    Toast.makeText(ChooseCounter.this,
                            "Please select Canteen",
                            Toast.LENGTH_SHORT).show();
                }
                else if (txtFromCounterSpinner.equals("Select Counter")){
                    Toast.makeText(ChooseCounter.this,
                            "Please select Counter",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    String ipAddress = "";
                    if (txtFromCanteenSpinner.equals("C1")){
                        charFromCanteenSpinner = "0";
                    }
                    else if (txtFromCanteenSpinner.equals("C2")){
                        charFromCanteenSpinner = "1";
                    }
                    else if (txtFromCanteenSpinner.equals("C3")){
                        charFromCanteenSpinner = "4";
                    }
                    else if (txtFromCanteenSpinner.equals("C4")){
                        charFromCanteenSpinner = "5";
                    }
                    else if (txtFromCanteenSpinner.equals("C5")){
                        charFromCanteenSpinner = "17";
                    }
                    else if (txtFromCanteenSpinner.equals("C6")){
                        charFromCanteenSpinner = "25";
                    }
                    else if(txtFromCanteenSpinner.equals("C7")){
                        charFromCanteenSpinner = "26";
                    }
                    else if(txtFromCanteenSpinner.equals("C8")){
                        charFromCanteenSpinner = "27";
                    }
                    else if(txtFromCanteenSpinner.equals("C9")){
                        charFromCanteenSpinner = "28";
                    }
                    else if(txtFromCanteenSpinner.equals("C10")){
                        charFromCanteenSpinner = "29";
                    }
                    txtFromCounterSpinner= txtFromCounterSpinner.replace("T","");
                    ipAddress = ipInitials+ charFromCanteenSpinner +"." + txtFromCounterSpinner;
                    Intent intent = new Intent();
                    intent.putExtra("ipAddressFromCounterScreen", ipAddress);
                    setResult(RESULT_OK, intent);
                    finish();
                }

            }


        });
    }
}
