package pos.rssb.com.posrssb;

import android.os.Parcel;
import android.os.Parcelable;

public class Model implements Parcelable {


    private String centerName;
    private String centerCode;
    private String areaName;
    private String areaCode;

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.centerName);
        dest.writeString(this.centerCode);
        dest.writeString(this.areaName);
        dest.writeString(this.areaCode);
    }

    public Model() {
    }

    protected Model(Parcel in) {
        this.centerName = in.readString();
        this.centerCode = in.readString();
        this.areaName = in.readString();
        this.areaCode = in.readString();
    }

    public static final Parcelable.Creator<Model> CREATOR = new Parcelable.Creator<Model>() {
        @Override
        public Model createFromParcel(Parcel source) {
            return new Model(source);
        }

        @Override
        public Model[] newArray(int size) {
            return new Model[size];
        }
    };
}
